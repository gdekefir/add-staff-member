import * as express from 'express';
import * as http from 'http';
import * as path from 'path';
// tslint:disable-next-line:no-require-imports
const auth: any = require('http-auth');

// tslint:disable-next-line:no-require-imports
const packageJson = require('../../../package.json');

const envVars = process.env;
const envVarStubs = packageJson.appSettings.envVarStubs;
const publicPath = pathFromRoot('public');
const app = express();
const port = envVars.PORT || envVarStubs.ASSETS_PORT;

if (!port) {throw('The "PORT" env variable must be set'); }

function pathFromRoot(url = '') {
  return path.resolve(__dirname, '../../..', url);
}

app.use(express.static(publicPath));

app.get('/', function(req, res){
  res.sendfile(`${publicPath}/index.html`);
});

http.createServer(app)
  .listen(port, function () {
    console.log(`Server is running on port ${port}`);
  });
