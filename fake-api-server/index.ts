import * as express from 'express';
import * as bodyParser from 'body-parser';

import staffMember from './endpoints/staff-member';
import formValidation from './endpoints/form-validation';

// tslint:disable-next-line:no-require-imports
const packageJson = require('../../../package.json');

const app = express();
const port = packageJson.appSettings.envVarStubs.API_PORT;
const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.header('Access-Control-Allow-Methods', 'GET, POST, HEAD, OPTIONS, PUT, DELETE, PATCH');
  next();
});

staffMember(app, urlencodedParser);
formValidation(app, urlencodedParser);


app.listen(port, function () {
  console.log(`JSON Server is running on port ${port}`);
});
