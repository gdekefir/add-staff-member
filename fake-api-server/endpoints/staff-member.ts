import sendWithTimeout from '../helpers/send-with-timeout';
import {Application, RequestHandler} from 'express-serve-static-core';
import {STATUS_OK} from '../../src/constants/used-http-status-codes';

import {CustomRequest} from '../interfaces/index';
import {RequestStaffMemberPayload} from '../../src/interfaces/api-requests';
import {ResponseStaffMemberCreateSuccessPayload} from '../../src/interfaces/api-responses';
import {urlStaffMembers} from '../../src/constants/urls';

interface Params {
  readonly token: string;
}

const staffMember = (server: Application, bodyParser: RequestHandler) => {
  server.get(urlStaffMembers, bodyParser, sendWithTimeout(500, (req: CustomRequest<RequestStaffMemberPayload, Params, any>, res) => {

    const response: ResponseStaffMemberCreateSuccessPayload = {
      staff_member_id: 777777
    };

    res
      .status(STATUS_OK)
      .send(response);

  }));
};

export default staffMember;
