import sendWithTimeout from '../helpers/send-with-timeout';
import {Application, RequestHandler} from 'express-serve-static-core';
import {STATUS_OK, STATUS_UNPROCESSABLE_ENTITY} from '../../src/constants/used-http-status-codes';

import {CustomRequest} from '../interfaces/index';
import {RequestFormValidatePayload} from '../../src/interfaces/api-requests';
import {ResponseFieldsValidationErrorsPayload} from '../../src/interfaces/api-responses';
import {urlValidateForm} from '../../src/constants/urls';

interface Params {
  readonly token: string;
}

const formValidation = (server: Application, bodyParser: RequestHandler) => {
  server.get(urlValidateForm, bodyParser, sendWithTimeout(500, (req: CustomRequest<RequestFormValidatePayload, Params, any>, res) => {

    const response: ResponseFieldsValidationErrorsPayload = {
      base: [
        'first error message',
        'second error message'
      ],
      field_name_1: [
        'some error',
        'other error'
      ]
    };

    res
      .status(STATUS_UNPROCESSABLE_ENTITY)
      .send(response);

  }));
};

export default formValidation;
