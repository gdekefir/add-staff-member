export const isRequiredField = 'This is a required field!';
export const isWrongEmail = 'It should be a correct email address';
export const isPhoneNumber = 'It should be a correct phone number';
