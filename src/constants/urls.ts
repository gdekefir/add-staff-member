const versionPrefix = 'v1';

export const urlStaffMembers = `/api/${versionPrefix}/staff_members.json`;
export const urlValidateForm = `/api/${versionPrefix}/validate_form.json`;

