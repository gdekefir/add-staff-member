import {REGISTRATION_STEP_BACK} from '../constants/action-names';
import {createSimpleAction} from '../helpers/actions';

const registrationStepBack = createSimpleAction(REGISTRATION_STEP_BACK);

export default registrationStepBack;
